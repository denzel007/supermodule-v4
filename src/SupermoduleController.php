<?php

namespace Supermodule;

use \DataTables\DataTable;
use App\Models\Modules;

class SupermoduleController extends ControllerBase
{

    public function initialize()
    {
      $this->view->setTemplateBefore('main');    
    }
    
    public function indexAction()
    {
         $this->view->title = 'Dashboard';
         $this->view->logo = 'Supermodule';
         $this->view->header = 'All Modules';
         $this->view->buttonLink = 'index/genAll';
    }
    
    public function allModulesAction() 
    {
        $modules = [];
        
        $modules  = $this->modelsManager->createQuery("SELECT * FROM \App\Models\Modules")
                             ->execute()->toArray();
             $arrayTests = ['emailer', 'upload', 'multilang', 'multitimezone', 'pager', 
                 'search', 'validfiltr', 'flashmsg', 'baseapi'
                 ];
             $arrayNotOff = ['roles'];
             
             foreach ($modules as $key => &$value) {
                $this->view->id = $value['id'];
                if (in_array($value['name'], $arrayTests)) {
                 $modules[$key]['name'] =  $this->view->getPartial('_partials/link',['name'=>'tests/'.$value['name']]);
                 $modules[$key]['operation'] = $this->view->getPartial('_partials/operations', ['status'=>$value['status'], 'id' =>$value['id'] ]);
                 } elseif (in_array($value['name'], $arrayNotOff)) {
                    $modules[$key]['operation'] = 'operation is not available';
                 }
                 else {
                   $modules[$key]['name'] =  $this->view->getPartial('_partials/link',['name'=> $value['name']]);  
                   $modules[$key]['operation'] = $this->view->getPartial('_partials/operations', ['status'=>$value['status'], 'id' =>$value['id'] ]);
                 }
                
             }
        
        $dataTables = new DataTable();
        $dataTables->fromArray($modules)->sendResponse(); 
    }
    
    public function changeAction($status = 'off', $moduleId = false) 
    {
        $statuses = ['on', 'off'];
        
        $module = Modules::findFirst($moduleId);
        
        if (in_array($status, $statuses)) {
          $module->status = $status;
          if ($module->save()) {
              return $this->response->redirect('supermodule');
          }
        }
        
    }
}


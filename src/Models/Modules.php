<?php

namespace Supermodule\Models;

use \Phalcon\Mvc\Model\ResultsetInterface;

class Modules extends \Phalcon\Mvc\Model
{

    public function initialize()
    {
         $this->setSource('modules');
    }

}

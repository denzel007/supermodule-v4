<?php

namespace Supermodule;

use Phalcon\Mvc\Controller;

use Phalcon\Di;

class ControllerBase extends Controller
{
   
   protected $di;

   public static function checkAndConnectModule($controller = false)
   {
        if (!empty($_SERVER['REQUEST_URI'])) {
            
          $modulesConfig = Di::getDefault()->get('config')->modules;
           
           # default controller (in custom module)
           if (!$controller) {
             $controller = 'index';  
           }
           $controller = strtolower($controller);
          
           # scan all modules
           $modulesDirs = scandir(MODULES_PATH);
           
           # add default
           array_push ( $modulesDirs , 'index');
           
           if (!in_array($controller, $modulesDirs)) {
               return false;
           }
                   
                   
           if (($controller != 'index')AND($controller != 'supermodule')) {
             
            $onOffConfigFlag = false;   
               
            if(isset($modulesConfig->$controller->onOff) AND $modulesConfig->$controller->onOff === 'off') {
                $onOffConfigFlag = true;
            } 
               
            # get module status   
            $module = \App\Models\Modules::findFirst("name = '$controller'"); 
            if ($module->status == 'off' OR $onOffConfigFlag) {
                return false;
            }
            
            if ($module->status == 'on') {
               // $this->$controller = true;
                return true;
            }

            return false;
            
           # if super module 
           } elseif ($controller == 'supermodule') {
             //$this->$controller = true;  
             return true;  
           } elseif ($controller == 'index'){
              // exit;
              return true;  
           }
          
        } 
        return false;
    }
}
